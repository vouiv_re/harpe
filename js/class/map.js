import { Tile } from "./tile.js";
import { Character } from "./character.js";
import { Camera } from './camera.js';

export class Map{
  constructor(height = 10, width = 10){
    this.height = height;
    this.width = width;

    this.grounds = [];
    this.creatures = [];
    this.sceneries = [];
    this.sunlight = [];

    this.camera = new Camera();
    this.turns = [];
  }

  init(){
    let w = this.width;
    let h = this.height;

    for (let x = 0; x < w; x++) {
      this.grounds.push([]);
      this.creatures.push([]);
      this.sceneries.push([]);
      this.sunlight.push([]);
      for (let y = 0; y < h; y++) {
        this.grounds[x].push([]);
        this.creatures[x].push([]);
        this.sceneries[x].push([]);
        this.sunlight[x].push([]);
        if (x === 0 || y === 0 || x === w - 1 || y === h - 1){
          this.grounds[x][y] = new Tile("step", true);
        } else {
          this.grounds[x][y] = new Tile("floor");
        }
      }
    }
    this.addObstacle(7, 3, 3, 2);
    this.addObstacle(7, 7, 5, 5);
    this.grounds[1][1] = new Tile("step", true);
    this.grounds[7][9] = new Tile("floor", false);
    this.camera.displayMap(this);
    //console.log(this.grounds);
  }

  // createMap() {
  //   this.grounds = this.createRectangle(this.width, this.height, new Tile("wall", true), new Tile("floor"));
  //   this.addObstacle(3, 3, 3, 2);
  //   this.addObstacle(7, 7, 2, 2);
  //   this.drawMap();
  // }

  createRectangle(w, h, border, content) {
    let rectangle = [];
    for (let x = 0; x < w; x++) {
      rectangle.push([]);
      for (let y = 0; y < h; y++) {
        if (x === 0 || y === 0 || x === w - 1 || y === h - 1){
          rectangle[x].push(border);
        } else {
          rectangle[x].push(content);
        }
      }
    }
    return rectangle;
  }

  // debugMap(array = this.matrix) {
  //   for (let i = 0; i < array.length; i++) {
  //     let row = "";
  //     for (let y = 0; y < array[i].length; y++) {
  //       if (array[i][y].type === "wall") {
  //         row += "_";
  //       } else if (array[i][y].type === "floor") {
  //         row += "-";
  //       }
  //     }
  //     console.log(row);
  //   }
  // }

   isObstacle(keyTouch) {
     let x = this.camera.center[0];
     let y = this.camera.center[1];
     if (keyTouch === "ArrowDown") {
       x += 1;
       if (this.grounds[x][y].obstacle === false) {
         this.camera.center[0] += 1;
         this.creatures[x][y] = this.creatures[x - 1][y];
         this.creatures[x - 1][y] = [];
       } else {
         console.log("Obstacle");
       }
     } else if (keyTouch === "ArrowUp") {
       x -= 1;
       if (this.grounds[x][y].obstacle === false) {
         this.camera.center[0] -= 1;
         this.creatures[x][y] = this.creatures[x + 1][y];
         this.creatures[x + 1][y] = [];
       } else {
         console.log("Obstacle");
       }
     } else if (keyTouch === "ArrowRight") {
       y += 1;
       if (this.grounds[x][y].obstacle === false) {
         this.camera.center[1] += 1;
         this.creatures[x][y] = this.creatures[x][y - 1];
         this.creatures[x][y - 1] = [];
       } else {
         console.log("Obstacle");
       }
     } else if (keyTouch === "ArrowLeft") {
       y -= 1;
       if (this.grounds[x][y].obstacle === false) {
         this.camera.center[1] -= 1;
         this.creatures[x][y] = this.creatures[x][y + 1];
         this.creatures[x][y + 1] = [];
       } else {
         console.log("Obstacle");
       }
     }
   }

  // redraw(x, y, w, h){
  //   let bigObstacle = this.createRectangle( 4, 4, new Tile("wall", true), new Tile("wall", true));
  // }

  addObstacle(x, y, w, h){
    let bigObstacle = this.createRectangle( w, h, new Tile("step", true), new Tile("floor"));
    for (let i = 0; i < w; i++) {
      for (let j = 0; j < h; j++) {
        this.grounds[x + i][y + j] = bigObstacle[i][j];
      }
    }
  }
}